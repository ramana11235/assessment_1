# Virtualisation Marked Assessment 1

## Introduction

Insert Introduction here

## Requirements :
- Webservers:
  - Ubuntu Server 
    - Name: ramana-ubuntu
    - Project: assessment1
    - Ubuntu Server 18.04 LTS
    - 1GB RAM
    - 1 vCPU
    - Should have NGINX Installed
  - AWS Linux Server - using CentOS 
    - Name: ramana-aws
    - Project: assessment1
    - Amazon Linux 2 AMI (HVM)
    - 1GB RAM
    - 1 vCPU
    - Should have Apache Installed
  - Both Must Have:
    - User called: instructor
    - With Password: m4g1cAut0L0t10n
    - sshd must allow password log in
- Loadbalancer:
  - AWS Linux Server
    - Name: ramana-loadbalancer
    - Project: assessment1
    - Amazon Linux 2 AMI (HVM)
    - 1GB RAM
    - 1 vCPU
    - Should have haproxy installed

## Things Done:
1. Launched all 3 instance
   - 1 ubuntu webserver (pub ip `52.16.86.167`)
     - Who's going to be hosting website through `nginx`
   - 1 aws webserver (pub ip `34.243.254.212`)
     - Who's going to be hosting website through `Apache`
   - 1 ubuntu loadbalancer (pub ip `34.253.240.66`)
     - Loadbalancing will be done through `haproxy`
2. Provisioning aws server
   1. Install `apache` onto aws server
      - `sudo -y yum install httpd`
   2. Start the `apache` server
      - `sudo systemctl start httpd`
   3. Creating the html for .html file
      -   `awk 'FNR <=2' /etc/os-release` 
          -   Prints just the first two lines from that file which includes the name and version of operating system
      - `My IP address is $(ifconfig | grep -A1 eth0 | grep inet | awk '{print $2}'| sed 's/^.*://')`
        - Prints the private ip address of this server
      - `/usr/sbin/httpd -v | awk 'FNR <=1'` 
        - Prints the server software and the version 
      - NEED TO FINISH
3. Provisioning the `nginx` server on ubuntu 
   1. `sudo apt update`
   2. `sudo -y apt install nginx`
   3.  `sudo ufw allow "Nginx HTTP"` - this might have to be changed depending on firewall spec
   4.  `sudo systemctl start nginx` - should have started already, but just incase
   5.  Need to create an `index.html for this too`
       - U can reuse the HTML from the amazon server
       - `awk 'FNR <=2' /etc/os-release`
       - `My IP address is $(ifconfig | grep -A1 eth0 | grep inet | awk '{print $2}'| sed 's/^.*://')`
       - `nginx -v`


4. Connecting the loadbalancer with both webservers
   1. `sudo apt update` 
   2. `sudo add-apt-repository ppa:vbernat/haproxy-2.2`
   3. `sudo apt update` 
      - He ssaid do it twice to install latest packages on system
   4. `sudo apt-get install haproxy=2.2.\*`
   5. `haproxy -v` shows version number (should say `2.2.13`)
   6. To set up server for load balancing
      1. `sudo nano /etc/haproxy/haproxy.cfg`
      2. The `frontend` and `backend` needed to be added to the cfg automatically
      ```bash
      frontend simple_load_balancer
      bind *:80
      mode http
      stats enable
      stats uri /haproxy?stats
      stats realm Strictly\ Private
      stats auth A_Username:Admin
      stats auth Another_User:Admin
      default_backend simple_servers

      backend simple_servers
      balance roundrobin
      option httpclose
      option forwardfor
      server lamp1 172.31.1.53:80 check
      server lamp2 172.31.8.186:80 check
      ```
      1. The `server lamp` must be updated automatically depending on new machines `PRIVATE IP`, everything else can be copied and pasted
5. Creating users for aws and ubuntu server (THESE INSTRUCTIONS WORK FOR BOTH)
   1. `sudo useradd -m instructor`
   2. `sudo passwd instructor`
      1. And then type in the password `m4g1cAut0L0t10n`
   3. `su - instructor`
   4. `sudo nano /etc/ssh/sshd_config` to change passwords log in to yes
      1. For automation use the `sed` command 
         1. `sudo sed -i "s,PasswordAuthentication no,PasswordAuthentication yes," /etc/ssh/sshd_config`
      2. PasswordAuthentication yes
   5. `sudo systemctl reload sshd`
   6. Need to make user `instructor` a sudoer
      1. `sudo ls /etc/sudoers.d`
      2. `sudo nano /etc/sudoers.d/91-instructor`
      3. WRITE IN THERE: `instructor ALL=(ALL) ALL`